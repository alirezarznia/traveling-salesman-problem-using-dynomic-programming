//problem : https://ncna17.kattis.com/problems/pokemongogo

//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include <bits/stdc++.h>


#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        1000000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;


inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
        return 53 +(ch - '0');
	}
}
 //int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}

}
ll n ;
ll dp[40000][20];
vector<pair<pii , string > > v;
ll oe=0;
map<string , bool> mp;
map<string , ll> zz;
ll F(ll x , ll vis , ll nu){
    ll &ret = dp[vis][x];
    if(nu == oe) return abs(v[x].F.F)+abs(v[x].F.S);
    if(dp[vis][x] != inf) return ret;
    Rep(i ,n){
        ll xx = v[i].first.first ,yy =v[i].first.second;
        string ss = v[i].second;
        if(!(vis &(zz[ss])) && !mp[ss]){
            mp[ss]=true;
            ll dd;
            if(vis !=0)
             dd = abs(v[x].F.F-xx)+abs(v[x].F.S - yy);
            else dd = abs(xx)+abs(yy);
            ret = min(ret , dd+ F(i , vis|(zz[ss]) ,nu+1));
            mp[ss]=false;
        }
    }
    return ret;
}
int main(){
  //  Test;
    cin>>n;
    ll r = min(pow(2 , n) , pow(2, 15));
    Rep(i ,r){
        Rep(j ,n ) dp[i][j]=inf;
    }
    Rep(i ,n){
        ll x, y ;cin>>x>>y;
        string s; cin >> s;
        v.push_back(MP(MP(x, y)  , s));
        if(!zz[s]){
                    zz[s]=1<<oe;
                    oe++;
        }
    }
    cout<<F(0   , 0 , 0)<<endl;
}
